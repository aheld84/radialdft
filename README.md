# RadialDFT

RadialDFT is a density functional theory (DFT) code for potentials with spherical symmetry written by Alexander Held.
It is based on a Fortran program written by Michael Moseler.
RadialDFT is intended as an educational code that illustrates the basic principles of Kohn-Sham DFT.
It is mainly written in python.
For the Numerov integration, an optional C implementation is provided.

# CI Status

[![pipeline status](https://gitlab.com/aheld84/radialdft/badges/master/pipeline.svg)](https://gitlab.com/aheld84/radialdft/commits/master) [![coverage report](https://gitlab.com/aheld84/radialdft/badges/master/coverage.svg)](https://gitlab.com/aheld84/radialdft/commits/master)

# Installation

## Linux

Clone the git repository to your local machine:

```shell
cd DIRECTORY_WHERE_TO_CLONE
git clone https://gitlab.com/aheld84/radialdft.git
```

Substitute `DIRECTORY_WHERE_TO_CLONE` appropriately.
Install:

```shell
cd radialdft
python setup.py install
```

This will install the package globally.
You might need root permissions.
To install locally, either use `python setup.py install --user` or `python setup.py install --prefix=SOME_LOCATION`.
Follow the instructions from the output about setting `$PYTHONPATH` in case the installation fails due to the target directory not being in `$PYTHONPATH`.

The `libnumerov.so` shared library will be built on demand in the `~/.radialdft` directory during runtime.
The GNU gcc compiler is needed for that to work.
Note that there is no locking for the compilation, i.e. if you intend to run multiple threads or multiple processes for the same user, make sure, `libnumerov.so` has been compiled already, either by running a calculation that uses the `radialdft.numerov.CNumerovShooter` once or by manually compiling in the folder `~/.radialdft`.
For manual compilation, the source, header and make script are found in [radialdft/libnumerov](radialdft/libnumerov).
I did not provide the C part as a Python extension, since the educational purpose was to learn how to use an existing shared library.

### Running tests

Tests can be run in the root of the repository as follows:

```shell
py.test
```

This requires the `pytest` package which should be installed automatically as a dependency when following the installation instructions.

Continuous integration (CI) tests are run automatically using the gitlab CI feature with tox.
You can see the output here: https://gitlab.com/aheld84/radialdft/pipelines

## Other platforms

Installation was not tested on other platforms.
The pure python part should work similarly to the Linux instructions.
The shared library for the C-implementation of the Numerov shooting is inteded to work on Linux only.

# Usage examples

## Calculate ionization energy

The following example illustrates the usage of the code by calculating the 1st ionization energy of a Carbon atom:

```python
from __future__ import print_function, division
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft.mixer import LinearMixer
from radialdft.xc import LDA
from radialdft.occupations import AufbauPrincipleOccupations
from radialdft.ksdft import KSDFTCalculator, eV, Ang
from radialdft.potential import CorePotential
from radialdft.numerov import PyNumerovShooter, CNumerovShooter
from radialdft.poisson import PoissonSolver


# from David R. Lide (ed)
# CRC Handbook of Chemistry and Physics, 84th Edition.
# CRC Press. Boca Raton, Florida, 2003; Section 10
# as quoted on https://en.wikipedia.org/wiki/Ionization_energies_of_the_elements_(data_page)
IE_exp_eV = 11.26030


shooter = PyNumerovShooter()
# shooter = CNumerovShooter()  # Alternative C-implementation (way faster)

Z = 6  # Carbon
eps_low = -Z ** 2 / 2. * 1.05  # lower bound for eigenvalue bisection

calc = KSDFTCalculator(
    CorePotential(Z),  # potential of the nucleus
    LDA(),  # local density approximation
    EigenSolver(eps_low, -eps_low, 1e-6 * eV, 200, shooter),  # how to solve Schroedinger equation
    PoissonSolver(),  # how to calculate electrostatic potential
    AufbauPrincipleOccupations(Z),  # occupation of Kohn-Sham orbitals
    LinearMixer(0.2)  # linear mixing with 20 % new density
)
grid = RadialGrid(rmax=8 * Ang, dr=0.005 * Ang)  # equidistant radial grid
calc.set_grid(grid)
calc.run_selfconsistent_loop()
E_C = calc.E  # potential energy of neutral C
calc.occupations.set_Z(Z - 1)  # now steal one electron
calc.run_selfconsistent_loop()
E_C_plus = calc.E  # potential energy of C+
IE = E_C_plus - E_C  # ionization energy

print("1st ionization energy of Carbon:")
print("Experiment: %.2f eV" % (IE_exp_eV, ))
print("RadialDFT simulation with LDA approximation: %.2f eV" % (IE / eV, ))
```

The example can also be found here: [radialdft/examples/carbon_ionization.py](radialdft/examples/carbon_ionization.py).
Running the example should produce the following final output:

```

...

1st ionization energy of Carbon:
Experiment: 11.26 eV
RadialDFT simulation with LDA approximation: 10.97 eV
```

## Use eigensolver to calculate wavefunctions

The following example calculates and plots some hydrogen atom wavefunctions.
For the plotting, `matplotlib` is needed.

```python
from __future__ import print_function, division
from builtins import range
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft import numerov
from radialdft.test import hydrogen
import numpy as np
from scipy.special import sph_harm
from matplotlib import pyplot as pp

Z = 1  # Hydrogen
dr = 0.01  # grid spacing
rmax = 100.0  # grid length
nrmax = 5  # max radial quantum number
lmax = nrmax - 1  # max l quantum number
eps_low = -Z ** 2 / 2. * 1.05  # lower bound for eigenvalue bisection
eps_high = -eps_low  # upper bound for eigenvalue bisection
accuracy = 1e-9  # eigensolver accuracy
maxiter = 200  # max number of iterations

rg = RadialGrid(dr=dr, rmax=rmax)
vs = -Z / rg.r

shooter = numerov.PyNumerovShooter()
# shooter = numerov.CNumerovShooter()  # alternative C-implementation (way faster)
es = EigenSolver(eps_low, eps_high, accuracy, maxiter, shooter)
es.set_grid(rg)
g, epsilon = es.solve(vs, nrmax, lmax)

plot_radial = False  # set to True in order to plot also radial wave functions
print('n | n_r | l | log10(abs eigenval error)')
for n in range(1, nrmax + 1):
    for l in range(n):
        nr = n - l
        nnodes_radial = nr - 1
        err = np.abs(epsilon[nnodes_radial, l] - hydrogen.epsilon(n, Z))
        print(n, nr, l, '%.2f' % (np.log10(err), ))
        if plot_radial:
            pp.figure()
            pp.plot(rg.r, hydrogen.g(n, l, rg.r), c='blue', label='analytical')
            pp.plot(rg.r, g[nnodes_radial, l], '--', c='red', label='numerical')
            pp.title(r'$n_{\mathrm{r}}$=%s, $l$=%s' % (nr, l))
            pp.legend(loc='upper right')
            pp.xlabel(r'$r / \mathrm{a.u.}$')
            pp.ylabel(r'$g(r)$')


# 2d cross section of the full wavefunction
# use lower accuracy and extend for plotting
dr = 0.2
rmax = 40.0
xylow = -rmax - dr / 2.
z = 0.001  # cross section for fixed z
cmap = 'afmhot'
#cmap = 'gray_r'  # for black/white printing

x, y = np.mgrid[-rmax:rmax + dr / 2.:dr, -rmax:rmax + dr / 2.:dr]
r = (x ** 2 + y ** 2 + z ** 2) ** .5
theta = np.arctan2(z, (x ** 2 + y ** 2) ** .5)
phi = np.arctan2(y, x)

for n, l, m, vmax in ((3, 0, 0, 0.0002), (4, 2, 0, 0.0001), (4, 3, 1, 0.00005)):
    nr = n - l
    f_nrl = np.interp(r, rg.r, g[nr - 1, l]) / r
    Y_lm = sph_harm(m, l, theta, phi)
    varphi2 = np.absolute(f_nrl * Y_lm) ** 2
    pp.figure()
    pp.imshow(
        varphi2, cmap=cmap, vmin=0, vmax=vmax,
        extent=(xylow, -xylow, xylow, -xylow)
    )
    pp.colorbar()
    pp.title(r'$|\varphi_{%s, %s, %s}(x, y, z=%s)|^2$' % (nr, l, m, z))
    pp.xlabel(r'$x$ / a.u.')
    pp.ylabel(r'$y$ / a.u.')

pp.show()
```

The example can also be found here: [radialdft/examples/plot_hydrogen.py](radialdft/examples/plot_hydrogen.py).

The following image shows a slice of the absolute square of the hydrogen atom wavefunction for the radial quantum number 1, l = 3, m = 1 as calculated by the numerical eigensolver:

![Hydrogen wavefunction (abs squared) example](radialdft/examples/hydrogen_1_3_1.png)

## Find magic numbers of Na jellium clusters

The following example calculates and plots the second finite difference derivative of the energy with respect to number of atoms for jellium Na clusters.
Magic numbers correspond to local maxima in the second derivative.
For the plotting, `matplotlib` is needed.

```python
from __future__ import print_function, division
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft.mixer import LinearMixer
from radialdft.xc import LDA
from radialdft.occupations import SquareWellOccupations
from radialdft.ksdft import KSDFTCalculator, eV, Ang
from radialdft.potential import JelliumPotential
from radialdft.numerov import CNumerovShooter
from radialdft.poisson import PoissonSolver
import numpy as np
from matplotlib import pyplot as pp


N = 1
Z = N
r_s = 3.93  # Na
eps_low = -Z ** 2 / 2. * 1.05
calc = KSDFTCalculator(
    JelliumPotential(r_s, Z),
    LDA(),
    EigenSolver(eps_low, -eps_low, 1e-6 * eV, 200, CNumerovShooter()),
    PoissonSolver(),
    SquareWellOccupations(N),
    LinearMixer(0.2)
)
dr_approx = 0.01 * Ang

Ns = np.arange(0, 43)
Es = []
for N in Ns:
    Z = N
    calc.potential.set_Z(Z)
    calc.eigensolver.eps_low = -Z ** 2 / 2. * 1.05
    calc.eigensolver.eps_high = -calc.eigensolver.eps_low
    calc.occupations.set_Z(Z)
    R_jell = calc.potential.R_jell
    if R_jell == 0:
        dr = dr_approx
    else:
        dr = R_jell / (np.ceil(R_jell / dr_approx) + 1e-3)
    grid = RadialGrid(dr=dr, rmax=R_jell + 5.0 * Ang)
    calc.set_grid(grid)
    calc.run_selfconsistent_loop()
    Es.append(calc.E)

Es = np.array(Es)

Ns = Ns[1:-1]
delta2 = np.diff(Es, 2)

# magic numbers are local maxima in second derivative delta2:
print("First magic numbers for spherically symmetric Na jellium with square well occupations:")
magic_numbers = np.nonzero(np.logical_and(delta2[1:-1] > delta2[:-2], delta2[1:-1] > delta2[2:]))[0] + 2
for magic_number in magic_numbers:
    print(magic_number)

pp.plot(Ns, delta2, 's-')
pp.xlabel(r'$N$')
pp.ylabel(r'Central Finite Difference $\frac{\partial^2 E}{\partial N^2}$ [a.u.]')
pp.tight_layout()
pp.show()
```

The example can also be found here: [radialdft/examples/Na_jellium.py](radialdft/examples/Na_jellium.py).

The following plot shows the result:

![Magic numbers for Na jellium clusters](radialdft/examples/Na_jellium_magic_numbers.png)

# License

Copyright (C) 2019 Alexander Held

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

See [LICENSE](LICENSE) file for your copy of the GNU General Public License.
