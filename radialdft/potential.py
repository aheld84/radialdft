# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
import numpy as np
from radialdft.poisson import PoissonSolver


class CorePotential(object):
    def __init__(self, Z):
        self.Z = Z

    def set_grid(self, grid):
        self.grid = grid

    def print_info(self):
        print('core potential with Z = %r' % (self.Z, ))

    def v(self):
        return -self.Z / self.grid.r

    def V_0(self):
        return 0.0


class JelliumPotential(object):
    def __init__(self, r_s, Z, poissonsolver=None):
        self.r_s = r_s
        self.set_Z(Z)
        self.poissonsolver = poissonsolver or PoissonSolver()

    def set_Z(self, Z):
        self.Z = Z
        self.R_jell = self.r_s * self.Z ** (1. / 3.)

    def set_grid(self, grid):
        self.grid = grid
        self.poissonsolver.set_grid(grid)

    def print_info(self):
        print('jellium potential with r_s = %r and Z = %r' % (self.r_s, self.Z))
        print('R_jell = %r' % (self.R_jell, ))

    def v(self):
        self.n_jell = np.where(
            self.grid.r <= self.R_jell,
            3. / (4. * np.pi * self.r_s ** 3),
            .0
        )
        if self.Z > 0:  # allow also Z = 0 case
            self.n_jell *= (-self.Z / self.grid.integrate(self.n_jell))
        self.v_jell = self.poissonsolver.solve(self.n_jell)
        return self.v_jell

    def V_0(self):
        return 0.5 * self.grid.integrate(self.n_jell * self.v_jell)
