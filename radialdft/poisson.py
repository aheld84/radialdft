# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
import numpy as np


class PoissonSolver(object):
    def set_grid(self, grid):
        self.grid = grid

    def solve(self, n):
        ndV = n * self.grid.dV
        r = self.grid.r
        tmp = np.empty_like(r)
        tmp[0] = 0
        np.cumsum(ndV[:-1], out=tmp[1:])
        u_l = tmp / r
        u_r = np.cumsum((ndV / r)[::-1])[::-1]
        return u_l + u_r

    def print_info(self):
        print('open boundary cumulative sum integration')
