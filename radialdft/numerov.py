# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
from builtins import range
import numpy as np
import os


class NumerovShooter(object):
    def __init__(self, small=1e-10):
        self.small = small  # pretty much arbitrary, as long as small != 0

    def shoot_outward(self, g, t):
        raise NotImplementedError

    def shoot_inward(self, g, t, i_match):
        raise NotImplementedError

    def set_grid(self, grid):
        self.grid = grid
        self.t_fac = grid.dr ** 2 / 6.

    def set_potential(self, vl):
        # pad with zero for left boundary condition
        self.vl = np.empty(vl.size + 1)
        self.vl[0] = 0
        self.vl[1:] = vl

    def shoot(self, eps):
        dr = self.grid.dr
        t = self.t_fac * (self.vl - eps)
        g = np.empty(self.vl.size)

        # shoot outward + determine matching point

        # boundary condition s.th. g(0) = 0 and ascending
        g[0] = -self.small  # g at -dr/2
        g[1] = self.small   # g at dr/2
        i_last = self.shoot_outward(g, t)
        if i_last >= len(g) - 3:
            i_match = len(g) // 2
        else:
            i_match = i_last - 1
        g_match = g[i_match]
        g_matchm1 = g[i_match - 1]
        g_prime_l = ((1. - t[i_match + 1]) * g[i_match + 1] - (1. - t[i_match - 1]) * g[i_match - 1]) / dr

        # shoot inward + count number of nodes

        # boundary condition as if vl = 0
        g[-1] = self.small
        if eps >= 0:
            g[-2] = 2. * self.small
        else:
            g[-2] = self.small * np.exp(np.sqrt(-2 * eps) * self.grid.dr)

        nnodes = self.shoot_inward(g, t, i_match)

        # match inward and outward solution
        factor = g_match / g[i_match]
        g[i_match - 1:] *= factor
        g_prime_r = ((1. - t[i_match + 1]) * g[i_match + 1] - (1. - t[i_match - 1]) * g[i_match - 1]) / dr
        g[i_match - 1] = g_matchm1

        # normalize
        srnorm = ((g[1:] ** 2).sum() * dr) ** .5
        g /= srnorm
        g_prime_l /= srnorm
        g_prime_r /= srnorm

        delta = -g[i_match] * (g_prime_r - g_prime_l) / dr / 2.
        return g[1:], delta, nnodes


class PyNumerovShooter(NumerovShooter):
    def shoot_outward(self, g, t):
        for i in range(2, len(g)):
            g[i] = (1. / (1. - t[i])) * (g[i - 2] * (t[i - 2] - 1.) + g[i - 1] * (2. + 10. * t[i - 1]))
            if g[i - 1] < g[i - 2]:
                break
        return i - 1

    def shoot_inward(self, g, t, i_match):
        nnodes = 0
        last = i_match - 1
        for i in range(len(g) - 3, last - 1, -1):
            g[i] = (1. / (1. - t[i])) * ((t[i + 2] - 1.) * g[i + 2] + (2. + 10. * t[i + 1]) * g[i + 1])
            if i != last and g[i] * g[i + 1] < .0:
                nnodes = nnodes + 1
        return nnodes

    def print_info(self):
        print("Numerov shooter (pure python)")


__libnumerov = None


def make_libnumerov(libdir):
    import pkg_resources
    import shutil
    import subprocess
    import stat
    try:
        os.makedirs(libdir)
    except OSError as error:
        if error.errno != os.errno.EEXIST:
            raise
    for fname in ('numerov.h', 'numerov.c', 'make.sh'):
        with pkg_resources.resource_stream(__name__, os.path.join('libnumerov', fname)) as ihandle:
            with open(os.path.join(libdir, fname), 'wb') as ohandle:
                shutil.copyfileobj(ihandle, ohandle)
                if fname == 'make.sh':
                    mode = os.fstat(ohandle.fileno()).st_mode
                    mode |= stat.S_IXUSR
                    os.fchmod(ohandle.fileno(), stat.S_IMODE(mode))
    cmd = os.path.join(libdir, 'make.sh')
    proc = subprocess.Popen([cmd], cwd=libdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdoutdata, stderrdata = proc.communicate()
    retcode = proc.poll()
    if retcode != 0:
        raise subprocess.CalledProcessError(
            returncode=retcode,
            cmd=cmd,
            output=(
                'Failed to compile libnumerov using gcc. Maybe gcc is not available on your platform.\n'
                'STDOUT:\n' + stdoutdata + 'END STDOUT\n'
                + 'STDERR:\n' + stderrdata + 'END STDERR'
            )
        )


def load_libnumerov():
    global __libnumerov
    if __libnumerov is None:
        import ctypes
        from numpy.ctypeslib import ndpointer
        arrp = ndpointer(ctypes.c_double, 1, flags="C_CONTIGUOUS")
        libdir = os.path.join(os.path.expanduser('~'), '.radialdft')
        path = os.path.join(libdir, 'libnumerov.so')
        if not os.path.exists(path):
            make_libnumerov(libdir)
        libnumerov = ctypes.CDLL(path)
        libnumerov.shoot_outward.argtypes = [arrp, arrp, ctypes.c_int]
        libnumerov.shoot_outward.restype = ctypes.c_int
        libnumerov.shoot_inward.argtypes = [arrp, arrp, ctypes.c_int, ctypes.c_int]
        libnumerov.shoot_inward.restype = ctypes.c_int
        __libnumerov = libnumerov
    return __libnumerov


class CNumerovShooter(NumerovShooter):
    def __init__(self):
        NumerovShooter.__init__(self)
        self.libnumerov = load_libnumerov()

    def shoot_outward(self, psi, t):
        return self.libnumerov.shoot_outward(psi, t, len(psi))

    def shoot_inward(self, psi, t, i_match):
        return self.libnumerov.shoot_inward(psi, t, len(psi), i_match)

    def print_info(self):
        print('Numerov shooter (using libnumerov.so)')
