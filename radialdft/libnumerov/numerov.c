/*
 This file is part of radialdft - a radial DFT code for educational purposes
 Copyright (C) 2019 Alexander Held

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

int shoot_outward(double *psi, const double *t, const int len)
{
  int i = 2;
  const int last = len - 1;
  while (i <= last) {
    psi[i] = (1. / (1. - t[i])) * (psi[i - 2] * (t[i - 2] - 1.) + psi[i - 1] * (2. + 10. * t[i - 1]));
    ++i;
    if (psi[i - 1] < psi[i - 2]) break;
  }
  return i - 1;
}

int shoot_inward(double *psi, const double *t, const int len, const int i_match)
{
  int nnodes = 0;
  int i = len - 3;
  const int last = i_match - 1;
  while (i >= last) {
    psi[i] = (1. / (1. - t[i])) * ((t[i + 2] - 1.) * psi[i + 2] + (2. + 10. * t[i + 1]) * psi[i + 1]);
    if (i != last && psi[i] * psi[i + 1] < .0) ++nnodes;
    --i;
  }
  return nnodes;
}
