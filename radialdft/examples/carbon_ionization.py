# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft.mixer import LinearMixer
from radialdft.xc import LDA
from radialdft.occupations import AufbauPrincipleOccupations
from radialdft.ksdft import KSDFTCalculator, eV, Ang
from radialdft.potential import CorePotential
from radialdft.numerov import PyNumerovShooter, CNumerovShooter
from radialdft.poisson import PoissonSolver


# from David R. Lide (ed)
# CRC Handbook of Chemistry and Physics, 84th Edition.
# CRC Press. Boca Raton, Florida, 2003; Section 10
# as quoted on https://en.wikipedia.org/wiki/Ionization_energies_of_the_elements_(data_page)
IE_exp_eV = 11.26030


shooter = PyNumerovShooter()
# shooter = CNumerovShooter()  # Alternative C-implementation (way faster)

Z = 6  # Carbon
eps_low = -Z ** 2 / 2. * 1.05  # lower bound for eigenvalue bisection

calc = KSDFTCalculator(
    CorePotential(Z),  # potential of the nucleus
    LDA(),  # local density approximation
    EigenSolver(eps_low, -eps_low, 1e-6 * eV, 200, shooter),  # how to solve Schroedinger equation
    PoissonSolver(),  # how to calculate electrostatic potential
    AufbauPrincipleOccupations(Z),  # occupation of Kohn-Sham orbitals
    LinearMixer(0.2)  # linear mixing with 20 % new density
)
grid = RadialGrid(rmax=8 * Ang, dr=0.005 * Ang)  # equidistant radial grid
calc.set_grid(grid)
calc.run_selfconsistent_loop()
E_C = calc.E  # potential energy of neutral C
calc.occupations.set_Z(Z - 1)  # now steal one electron
calc.run_selfconsistent_loop()
E_C_plus = calc.E  # potential energy of C+
IE = E_C_plus - E_C  # ionization energy

print("1st ionization energy of Carbon:")
print("Experiment: %.2f eV" % (IE_exp_eV, ))
print("RadialDFT simulation with LDA approximation: %.2f eV" % (IE / eV, ))
