# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
from builtins import range
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft import numerov
from radialdft.test import hydrogen
import numpy as np
from scipy.special import sph_harm
from matplotlib import pyplot as pp

Z = 1  # Hydrogen
dr = 0.01  # grid spacing
rmax = 100.0  # grid length
nrmax = 5  # max radial quantum number
lmax = nrmax - 1  # max l quantum number
eps_low = -Z ** 2 / 2. * 1.05  # lower bound for eigenvalue bisection
eps_high = -eps_low  # upper bound for eigenvalue bisection
accuracy = 1e-9  # eigensolver accuracy
maxiter = 200  # max number of iterations

rg = RadialGrid(dr=dr, rmax=rmax)
vs = -Z / rg.r

shooter = numerov.PyNumerovShooter()
# shooter = numerov.CNumerovShooter()  # alternative C-implementation (way faster)
es = EigenSolver(eps_low, eps_high, accuracy, maxiter, shooter)
es.set_grid(rg)
g, epsilon = es.solve(vs, nrmax, lmax)

plot_radial = False  # set to True in order to plot also radial wave functions
print('n | n_r | l | log10(abs eigenval error)')
for n in range(1, nrmax + 1):
    for l in range(n):
        nr = n - l
        nnodes_radial = nr - 1
        err = np.abs(epsilon[nnodes_radial, l] - hydrogen.epsilon(n, Z))
        print(n, nr, l, '%.2f' % (np.log10(err), ))
        if plot_radial:
            pp.figure()
            pp.plot(rg.r, hydrogen.g(n, l, rg.r), c='blue', label='analytical')
            pp.plot(rg.r, g[nnodes_radial, l], '--', c='red', label='numerical')
            pp.title(r'$n_{\mathrm{r}}$=%s, $l$=%s' % (nr, l))
            pp.legend(loc='upper right')
            pp.xlabel(r'$r / \mathrm{a.u.}$')
            pp.ylabel(r'$g(r)$')


# 2d cross section of the full wavefunction
# use lower accuracy and extend for plotting
dr = 0.2
rmax = 40.0
xylow = -rmax - dr / 2.
z = 0.001  # cross section for fixed z
cmap = 'afmhot'
#cmap = 'gray_r'  # for black/white printing

x, y = np.mgrid[-rmax:rmax + dr / 2.:dr, -rmax:rmax + dr / 2.:dr]
r = (x ** 2 + y ** 2 + z ** 2) ** .5
theta = np.arctan2(z, (x ** 2 + y ** 2) ** .5)
phi = np.arctan2(y, x)

for n, l, m, vmax in ((3, 0, 0, 0.0002), (4, 2, 0, 0.0001), (4, 3, 1, 0.00005)):
    nr = n - l
    f_nrl = np.interp(r, rg.r, g[nr - 1, l]) / r
    Y_lm = sph_harm(m, l, theta, phi)
    varphi2 = np.absolute(f_nrl * Y_lm) ** 2
    pp.figure()
    pp.imshow(
        varphi2, cmap=cmap, vmin=0, vmax=vmax,
        extent=(xylow, -xylow, xylow, -xylow)
    )
    pp.colorbar()
    pp.title(r'$|\varphi_{%s, %s, %s}(x, y, z=%s)|^2$' % (nr, l, m, z))
    pp.xlabel(r'$x$ / a.u.')
    pp.ylabel(r'$y$ / a.u.')

pp.show()
