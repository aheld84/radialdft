# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function
from builtins import range
import numpy as np
import re
from radialdft.strformat import ul


def conf_to_occ_nr_l(s):
    if len(s.strip()) == 0:
        return np.zeros((1, 1))
    if s == s.lower():
        mode = 'atom'
    elif s == s.upper():
        mode = 'cluster'
    else:
        raise ValueError(s)
    s = s.lower()
    pattern = r'([0-9]+)([spdfghi])([0-9]+)\s+'
    matches = re.finditer(pattern, s + ' ')
    ns = []
    ls = []
    occs = []
    if matches:
        for match in matches:
            n, l, occ = match.groups()
            ns.append(int(n))
            ls.append('spdfghi'.index(l))
            occs.append(float(occ))
        occ_n_l = np.zeros((np.max(ns), np.max(ls) + 1))
        for n, l, occ in zip(ns, ls, occs):
            occ_n_l[n - 1, l] = occ
        if mode == 'atom':
            return occ_n_l_to_occ_nr_l(occ_n_l)
        elif mode == 'cluster':
            return occ_n_l
        else:
            raise ValueError(mode)
    else:
        raise ValueError(s)


def occ_n_l_to_occ_nr_l(occ_n_l):
    """convert principal to radial quantum number for occupations"""
    n, l = np.mgrid[1:occ_n_l.shape[0] + 1, 0:occ_n_l.shape[1]]
    nr = n - l
    assert (occ_n_l[nr < 1] == 0).all()
    occ_nr_l = np.zeros((nr.max(), l.max() + 1))
    nr = nr.flatten()
    l = l.flatten()
    occ = occ_n_l.flatten()
    mask = nr >= 1
    nr = nr[mask]
    l = l[mask]
    occ = occ[mask]
    occ_nr_l[nr - 1, l] = occ
    return occ_nr_l


class FixedOccupations(object):
    def __init__(self, occ_nr_l):
        self.set_occ_nr_l(occ_nr_l)

    def set_occ_nr_l(self, occ_nr_l):
        if occ_nr_l is None:
            return
        self.occ_nr_l = np.array(occ_nr_l, dtype=float)
        self.nrmax, self.lmax = self.occ_nr_l.shape
        self.lmax -= 1

    def get_occupation_numbers(self, epsilon):
        return self.occ_nr_l

    def set_grid(self, grid):
        pass

    def print_info(self):
        print('fixed occupations')
        print(ul('  n_r |   l   |  occ. '))
        for nr in range(1, self.nrmax + 1):
            for l in range(self.lmax + 1):
                print (
                    '%s | %s | %6.3f' % (
                        str(nr).rjust(5),
                        str(l).rjust(5),
                        self.occ_nr_l[nr - 1, l]
                    )
                )


class ConfigurationOccupations(FixedOccupations):
    def __init__(self, conf):
        self.set_conf(conf)

    def set_conf(self, conf):
        self.conf = conf
        if conf is None:
            return
        FixedOccupations.set_occ_nr_l(self, conf_to_occ_nr_l(conf))

    def print_info(self):
        print('fixed configuration occupations')
        print('configuration: ' + self.conf)


class AufbauPrincipleOccupations(ConfigurationOccupations):
    order = '1s 2s 2p 3s 3p 4s 3d 4p 5s 4d 5p 6s 4f 5d 6p 7s 5f 6d 7p'.split()
    degeneracy = [{'s': 2, 'p': 6, 'd': 10, 'f': 14}[o[1]] for o in order]

    def __init__(self, Z):
        self.set_Z(Z)

    def set_Z(self, Z):
        self.Z = Z
        if Z is None:
            return
        conf = ''
        N = 0
        for shell, g in zip(self.order, self.degeneracy):
            N_left = Z - N
            if N_left <= 0:
                break
            N_shell = min(N_left, g)
            N += N_shell
            conf += ' ' + shell + str(N_shell)
        ConfigurationOccupations.set_conf(self, conf.strip())


class SquareWellOccupations(AufbauPrincipleOccupations):
    """From W. de Heer, Rev. Mod. Phys. 65, 3, 611 (1993)"""

    order = '1S 1P 1D 2S 1F 2P 1G 2D 1H 3S 2F 1I 3P 2G'.split()
    degeneracy = [{'S': 2, 'P': 6, 'D': 10, 'F': 14, 'G': 18, 'H': 22, 'I': 24}[o[1]] for o in order]
