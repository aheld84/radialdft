# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division
from builtins import range
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft.numerov import PyNumerovShooter, CNumerovShooter
from radialdft.test import hydrogen
import numpy as np
import pytest


@pytest.mark.parametrize("shooter_factory", (PyNumerovShooter, CNumerovShooter))
def test_hydrogen_eigenvalues(shooter_factory):
    shooter = shooter_factory()
    Z = 1
    dr = 0.01
    rmax = 60.0
    eps_low = -Z ** 2 / 2. * 1.05
    eps_high = -eps_low
    accuracy = 1e-9
    maxiter = 200

    rg = RadialGrid(dr=dr, rmax=rmax)
    vs = -Z / rg.r

    es = EigenSolver(eps_low, eps_high, accuracy, maxiter, shooter)
    es.set_grid(rg)
    g, epsilon = es.solve(vs, 2, 1)

    for nr in range(1, 3):
        for l in range(2):
            n = nr + l
            err = np.abs(epsilon[nr - 1, l] - hydrogen.epsilon(n, Z))
            assert err <= 1e-4
