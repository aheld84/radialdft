# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft.mixer import LinearMixer
from radialdft.xc import LDA
from radialdft.occupations import AufbauPrincipleOccupations
from radialdft.ksdft import KSDFTCalculator, eV, Ang
from radialdft.potential import CorePotential
from radialdft.numerov import PyNumerovShooter, CNumerovShooter
from radialdft.poisson import PoissonSolver
import pytest


# from David R. Lide (ed)
# CRC Handbook of Chemistry and Physics, 84th Edition.
# CRC Press. Boca Raton, Florida, 2003; Section 10
# as quoted on https://en.wikipedia.org/wiki/Ionization_energies_of_the_elements_(data_page)
IE_exp_eV = 21.5646

@pytest.mark.parametrize("shooter_factory", (PyNumerovShooter, CNumerovShooter))
def test_neon_ionization_energy(shooter_factory):
    shooter = shooter_factory()
    Z = 10  # Neon
    dE = 0.1 * eV  # speed up test
    eps_low = -Z ** 2 / 2. * 1.05

    calc = KSDFTCalculator(
        CorePotential(Z),
        LDA(),
        EigenSolver(eps_low, -eps_low, 1e-6 * eV, 200, shooter),
        PoissonSolver(),
        AufbauPrincipleOccupations(Z),
        LinearMixer(0.2)
    )
    grid = RadialGrid(rmax=8 * Ang, dr=0.005 * Ang)
    calc.set_grid(grid)
    calc.run_selfconsistent_loop(dE=dE)
    E_Ne = calc.E
    calc.occupations.set_Z(Z - 1)
    calc.run_selfconsistent_loop(dE=dE)
    E_Ne_plus = calc.E
    IE = E_Ne_plus - E_Ne

    relative_error = (IE / eV - IE_exp_eV) / IE_exp_eV
    assert abs(relative_error) <= 0.05
