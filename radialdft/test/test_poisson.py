# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division
from radialdft.radial import RadialGrid
from radialdft.poisson import PoissonSolver
from scipy.special import erf
from scipy.stats import linregress
import numpy as np


def discretize_n(r):
    return np.pi ** (-3. / 2.) * np.exp(-r ** 2)


def discretize_u(r):
    return erf(r) / r


def get_u_uref_err(grid, psolver):
    n = discretize_n(grid.r)
    uref = discretize_u(grid.r)
    psolver.set_grid(grid)
    u = psolver.solve(n)
    return u, uref, np.abs(u - uref).max()


def test_poisson_error():
    psolver = PoissonSolver()
    rmax = 10.0
    dr = 0.01
    grid = RadialGrid(dr=dr, rmax=rmax)
    u, uref, err = get_u_uref_err(grid, psolver)
    assert err <= 1e-4


def test_poisson_error_convergence():
    psolver = PoissonSolver()
    rmax = 10.0
    drs = 10. ** np.linspace(-3, 0, 11)
    grids = [RadialGrid(dr=dr, rmax=rmax) for dr in drs]
    errors = np.array([get_u_uref_err(grid, psolver)[2] for grid in grids])
    x = np.log10(drs)
    y = np.log10(errors)
    slope, intercept = linregress(x[:5], y[:5])[:2]
    slope_error = np.abs(slope - 2)
    assert slope_error <= 1e-3
