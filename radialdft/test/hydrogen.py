# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division
import numpy as np
from scipy.special import genlaguerre
from math import factorial as fac


def f(n, l, r, Z=1):
    rho = 2. * Z * r / n
    prefac = np.sqrt(
        (2. * Z / n) ** 3 * fac(n - l - 1) / (2. * n * fac(n + l))
    )
    L = genlaguerre(n - l - 1, 2 * l + 1)
    return prefac * np.exp(-rho / 2.) * rho ** l * L(rho)


def g(n, l, r, Z=1):
    return r * f(n, l, r, Z)


def epsilon(n, Z=1):
    return -0.5 * Z ** 2 / n ** 2
