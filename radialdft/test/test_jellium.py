# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
from radialdft.radial import RadialGrid
from radialdft.eigensolver import EigenSolver
from radialdft.mixer import LinearMixer
from radialdft.xc import LDA
from radialdft.occupations import SquareWellOccupations
from radialdft.ksdft import KSDFTCalculator, eV, Ang
from radialdft.potential import JelliumPotential
from radialdft.numerov import CNumerovShooter
from radialdft.poisson import PoissonSolver
import numpy as np


def test_Na_magic_numbers():
    N = 1
    Z = N
    r_s = 3.93  # Na
    eps_low = -Z ** 2 / 2. * 1.05
    calc = KSDFTCalculator(
        JelliumPotential(r_s, Z),
        LDA(),
        EigenSolver(eps_low, -eps_low, 1e-6 * eV, 200, CNumerovShooter()),
        PoissonSolver(),
        SquareWellOccupations(N),
        LinearMixer(0.2)
    )
    dr_approx = 0.01 * Ang

    Ns = np.arange(0, 43)
    Es = []
    for N in Ns:
        Z = N
        calc.potential.set_Z(Z)
        calc.eigensolver.eps_low = -Z ** 2 / 2. * 1.05
        calc.eigensolver.eps_high = -calc.eigensolver.eps_low
        calc.occupations.set_Z(Z)
        R_jell = calc.potential.R_jell
        if R_jell == 0:
            dr = dr_approx
        else:
            dr = R_jell / (np.ceil(R_jell / dr_approx) + 1e-3)
        grid = RadialGrid(dr=dr, rmax=R_jell + 5.0 * Ang)
        calc.set_grid(grid)
        calc.run_selfconsistent_loop()
        Es.append(calc.E)

    Es = np.array(Es)
    delta2 = np.diff(Es, 2)
    # magic numbers are local maxima in second derivative delta2:
    magic_numbers = np.nonzero(np.logical_and(delta2[1:-1] > delta2[:-2], delta2[1:-1] > delta2[2:]))[0] + 2
    assert (magic_numbers == np.array([2, 8, 18, 20, 34, 40])).all()
