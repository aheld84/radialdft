# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
from builtins import range
import numpy as np
import time
import socket
import datetime
import timeit
from radialdft.strformat import ul
import pkg_resources

# internal units are Hartree energy and Bohr radius
eV = 1. / 27.21138602
Ang = 1. / 0.52917721067


class KSDFTCalculator(object):
    def __init__(
            self, potential, xc, eigensolver, poissonsolver, occupations, mixer
    ):
        self.potential = potential
        self.xc = xc
        self.eigensolver = eigensolver
        self.poissonsolver = poissonsolver
        self.occupations = occupations
        self.mixer = mixer
        self.component_names = [
            'External Potential',
            'XC Approximation',
            'Eigensolver',
            'Poisson Solver',
            'Occupations',
            'Mixer'
        ]

    @property
    def components(self):
        return (
            self.potential,
            self.xc,
            self.eigensolver,
            self.poissonsolver,
            self.occupations,
            self.mixer
        )

    def set_grid(self, grid):
        self.grid = grid
        for component in self.components:
            component.set_grid(grid)

    def print_info(self):
        version = pkg_resources.resource_string(__name__, 'VERSION').strip()
        vstring = version.ljust(31)
        print(r'/=================================================\\'[:-1])
        print(r'|RadialDFT Version %s|' % (vstring, ))
        print(r'|Kohn-Sham DFT Calculation with Spherical Symmetry|')
        print(r'\=================================================/')
        print('')
        print('''Copyright (C) 2019 Alexander Held

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''
        )
        print('')
        print('units: Hartree energy and Bohr radius')
        print('')
        print('date and time: %s' % (time.strftime("%c"), ))
        print('running on: %s' % (socket.gethostname(), ))
        print('')
        print(ul('Radialgrid:'))
        self.grid.print_info()
        print('')
        for component, name in zip(self.components, self.component_names):
            print(ul(name + ':'))
            component.print_info()
            print('')

    def run_selfconsistent_loop(self, dE=1e-3 * eV, maxiter=500):
        t0 = timeit.default_timer()
        self.print_info()
        print(ul('Convergence:'))
        print('dE: %r' % (dE, ))
        print('max. iterations: %r' % (maxiter, ))
        print('')

        # evaluate external potential
        self.v = self.potential.v()

        # initialize density
        self.n = np.zeros_like(self.grid.r)
        self.mixer.reset()
        self.mixer.mix(self.n)  # tell the mixer the initial density

        # iterate
        print(ul('# it. | Log10 Total Energy Error'))
        lastE = np.nan
        for self.niter in range(1, maxiter + 1):
            # 1. calculate Kohn-Sham potential from density
            self.u = self.poissonsolver.solve(self.n)
            self.v_xc = self.xc.v_xc(self.n)
            self.v_s = self.v + self.u + self.v_xc

            # 2. solve Kohn-Sham equation
            self.wfg, self.epsilon = self.eigensolver.solve(
                self.v_s, self.occupations.nrmax, self.occupations.lmax
            )
            self.wff = self.wfg / self.grid.r[None, None, :]

            # 3. calculate new density from Kohn-Sham orbitals
            self.occ_nr_l = self.occupations.get_occupation_numbers(self.epsilon)
            n_new = (self.occ_nr_l[..., None] * self.wff ** 2).sum((0, 1)) / (4. * np.pi)

            # mix density
            self.n = self.mixer.mix(n_new)

            # evaluate energies
            self.calculate_energies()
            error = -np.inf if self.E == lastE else np.log10(np.abs(self.E - lastE))
            if self.niter == 1:
                errorstr = ''
            else:
                errorstr = '%+6.2f' % (error, )
            lastE = self.E

            # print iteration info
            print('%s | %s' % (str(self.niter).rjust(5), errorstr))

            # check convergence
            if error <= np.log10(dE):
                self.finalize(t0, True)
                break
        else:
            self.finalize(t0, False)

    def finalize(self, t0, converged):
        t1 = timeit.default_timer()
        self.elapsed_time = datetime.timedelta(seconds=t1 - t0)
        if converged:
            self.print_converged()
        else:
            raise ValueError(
                'SC cycle did not converge within %s iterations!' % (
                    self.niter,
                )
            )

    def print_converged(self):
        print('')
        print('converged after %s iterations.' % (self.niter, ))
        print('')
        print('number of electrons: %r' % (self.grid.integrate(self.n), ))
        print('')
        print(ul('Kohn-Sham Orbitals:'))
        print(ul('  n_r |   l   |  occ.  |    eps     '))
        for nr in range(1, self.occupations.nrmax + 1):
            for l in range(self.occupations.lmax + 1):
                print(
                    '%s | %s | %6.3f | %+11.4f' % (
                        str(nr).rjust(5),
                        str(l).rjust(5),
                        self.occ_nr_l[nr - 1, l],
                        self.epsilon[nr - 1, l]
                    )
                )
        print('')
        print(ul('Energies:'))
        print('Kohn-Sham kinetic energy T_s   = %+11.4f' % (self.T_s, ))
        print('Hartree energy U               = %+11.4f' % (self.U, ))
        print('xc energy E_xc                 = %+11.4f' % (self.E_xc, ))
        print('potential energy V             = %+11.4f' % (self.V, ))
        print('potential energy V_0           = %+11.4f' % (self.V_0, ))
        print('--------------------------------------------')
        print('total energy E                 = %+11.4f' % (self.E, ))
        print('')
        print('Kohn-Sham potential energy V_s = %+11.4f' % (self.V_s, ))
        print('Kohn-Sham total energy E_s     = %+11.4f' % (self.E_s, ))
        print('')
        print('elapsed time: %s' % (self.elapsed_time, ))
        print('')
        print('Kohn-Sham DFT calculation finished')
        print('=' * 78)
        print('')
        print('')

    def calculate_energies(self):
        self.U = 0.5 * self.grid.integrate(self.n * self.u)
        self.V = self.grid.integrate(self.n * self.v)
        self.V_0 = self.potential.V_0()
        self.V_s = self.grid.integrate(self.n * self.v_s)
        self.E_xc = self.xc.E_xc(self.n)
        self.E_s = (self.occ_nr_l * self.epsilon).sum()
        self.T_s = self.E_s - self.V_s
        self.E = self.T_s + self.U + self.E_xc + self.V + self.V_0
