# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function


class LinearMixer(object):
    def __init__(self, alpha):
        self.alpha = alpha
        self.reset()

    def reset(self):
        self.n_old = None

    def mix(self, n):
        if self.n_old is None:
            self.n_old = n.copy()
            return self.n_old
        n_mixed = self.alpha * n + (1. - self.alpha) * self.n_old
        self.n_old = n_mixed
        return n_mixed

    def set_grid(self, grid):
        self.grid = grid

    def print_info(self):
        print('linear mixer with alpha=%r' % (self.alpha, ))
