# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
from builtins import range
import numpy as np


class EigenSolver(object):
    def __init__(self, eps_low, eps_high, accuracy, maxiter, integrator):
        self.eps_low = eps_low
        self.eps_high = eps_high
        self.accuracy = accuracy
        self.maxiter = maxiter
        self.integrator = integrator

    def set_grid(self, grid):
        self.grid = grid
        self.one_over_r2 = 1. / grid.r ** 2
        self.integrator.set_grid(grid)

    def print_info(self):
        print('bisecting eigensolver')
        print('eps_low = %r' % (self.eps_low, ))
        print('eps_high = %r' % (self.eps_high, ))
        print('accuracy = %r' % (self.accuracy, ))
        print('maxiter = %r' % (self.maxiter, ))
        print('Integrator:')
        self.integrator.print_info()

    def bisect(self, nnodes_desired, vl):
        self.integrator.set_potential(vl)
        eps_high = self.eps_high
        eps_low = self.eps_low
        for i in range(self.maxiter):
            eps = 0.5 * (eps_high + eps_low)
            g, delta, nnodes = self.integrator.shoot(eps)
            # test for convergence
            if np.abs(eps_high - eps_low) <= self.accuracy:
                if nnodes != nnodes_desired:
                    raise ValueError(
                        'Accuracy of %r reached, but #nodes = %s != %s!' % (
                            self.accuracy, nnodes, nnodes_desired
                        )
                    )
                return g, eps
            # update interval
            if nnodes == nnodes_desired:
                if delta > .0:
                    eps_low = eps
                elif delta < .0:
                    eps_high = eps
                else:
                    raise ValueError(
                        'delta=0.0, but desired accuracy not reached!'
                    )
            elif nnodes > nnodes_desired:
                eps_high = eps
            else:
                eps_low = eps
        raise ValueError(
            'Shooting solver did not converge within %s iterations!' % (
                self.maxiter,
            )
        )

    def solve(self, vs, nrmax, lmax):
        epsilon = np.empty((nrmax, lmax + 1))
        g = np.empty((nrmax, lmax + 1, self.grid.r.size))
        for l in range(lmax + 1):
            vl = vs + l * (l + 1.) / 2. * self.one_over_r2
            for nnodes in range(nrmax):
                g[nnodes, l, :], epsilon[nnodes, l] = self.bisect(nnodes, vl)
        return g, epsilon
