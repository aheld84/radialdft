# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
import numpy as np


class RadialGrid(object):
    def __init__(self, dr, rmax):
        self.dr = dr
        self.rmax = rmax
        self.r = np.arange(dr / 2., rmax, dr)
        self.dV = 4. * np.pi * self.r ** 2 * self.dr

    def integrate(self, f):
        return (f * self.dV).sum()

    def print_info(self):
        print('Equidistant radial grid')
        print('dr = %r' % (self.dr, ))
        print('rmax = %r' % (self.rmax, ))
