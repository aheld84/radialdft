# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division
import numpy as np


class LDA(object):
    def __init__(self, eps=1e-11):
        self.eps = eps
        self.a1 = 0.21370
        self.c0 = 0.031091
        self.c1 = 0.046644
        self.b1 = 1. / 2. / self.c0 * np.exp(-self.c1 / 2. / self.c0)
        self.b2 = 2. * self.c0 * self.b1 ** 2
        self.b3 = 1.6382
        self.b4 = 0.49294
        self.cx = -3. / 4. * (3. / (2. * np.pi)) ** (2. / 3.)

    def print_info(self):
        print('LDA')
        print('finite difference epsilon = %r' % (self.eps, ))

    def set_grid(self, grid):
        self.grid = grid

    def E_xc(self, n):
        return self.grid.integrate(n * self.e_xc(n))

    def v_xc(self, n):
        s = self
        nplus = n + s.eps
        nminus = n - s.eps
        v_xc = (nplus * s.e_xc(nplus) - nminus * s.e_xc(nminus)) / (2. * s.eps)
        v_xc[n <= 0] = .0  # r_s -> infinity, vanishing density
        return v_xc

    def rs(self, n):
        # avoid division by zero
        # also, n may be negative due to numerical derivative
        mask = n <= 0
        n = np.where(mask, 1.0, n)
        rs = (3. / (4. * np.pi * n)) ** (1. / 3.)
        rs[mask] = np.inf
        return rs

    def e_x(self, n):
        # finite value / np.inf is zero
        return self.cx / self.rs(n)

    def e_c(self, n):
        s = self
        rs = s.rs(n)
        mask = rs == np.inf
        rs = np.where(mask, 1.0, rs)
        e_c = -2. * s.c0 * (1. + s.a1 * rs) * np.log(
            1. + 1. / (
                2. * s.c0 * (s.b1 * np.sqrt(rs) + s.b2 * rs + s.b3 * rs ** (3. / 2.) + s.b4 * rs ** 2)
            )
        )
        e_c[mask] = .0  # n = 0
        return e_c

    def e_xc(self, n):
        return self.e_x(n) + self.e_c(n)
