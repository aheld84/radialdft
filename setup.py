# This file is part of radialdft - a radial DFT code for educational purposes
# Copyright (C) 2019 Alexander Held
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import setuptools
import os

with open("README.md", "r") as fh:
    long_description = fh.read()

with open(os.path.join("radialdft", "VERSION"), "r") as fh:
    version = fh.read().strip()

setuptools.setup(
    name="radialdft",
    version=version,
    author="Alexander Held",
    author_email="ah_privat@posteo.de",
    description="An educational radial density functional theory implementation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aheld84/radialdft",
    packages=setuptools.find_packages(),
    package_data={
        'radialdft': [
            'libnumerov/numerov.h',
            'libnumerov/numerov.c',
            'libnumerov/make.sh',
            'VERSION',
        ]
    },
    include_package_data=True,
    keywords="DFT",
    python_requires='>=2.7',
    install_requires=['pytest', 'numpy', 'scipy', 'future'],
    license="GNU General Public License v3 (GPLv3)",
    classifiers=[
        "Topic :: Scientific/Engineering :: Physics",
        "Natural Language :: English",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python",
        "Programming Language :: C",
        "Operating System :: OS Independent",
    ],
)
